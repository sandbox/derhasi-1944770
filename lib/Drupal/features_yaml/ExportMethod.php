<?php

/**
 * @file
 * Contains \Drupal\features_yaml\ExportMethod.
 */

namespace Drupal\features_yaml;


/**
 * Wrapper export class for symbolizing function calls.
 */
class ExportMethod extends ExportObject {
  /**
   * Overrides \Drupal\features_yaml\ExportObject->_object_type;
   * @var string
   */
  var $_object_type = 'function';

  /**
   * Overrides \Drupal\features_yaml\ExportObject::getConstructorCall().
   * @return string
   */
  public function getConstructorCall() {
    $call = 'function ' . $this->_name . '(';
    if (count($this->_arguments)) {
      $call .= json_encode($this->_arguments);
    }
    return $call . ')';
  }
}
