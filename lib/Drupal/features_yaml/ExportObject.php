<?php

/**
 * @file
 * Contains \Drupal\features_yaml\ExportObject.
 */

namespace Drupal\features_yaml;

/**
 * new myObject() goes to ExObject::myObject()
 * functionname() goes to ExportMethod::functionname().
 */

/**
 *
 */
class ExportObject implements \ArrayAccess {
  /**
   * Type of the export object.
   *
   * @var string
   */
  protected $_object_type = 'object';

  /**
   * Name of the class.
   *
   * @var string
   */
  protected $_name = 'stdClass';

  /**
   * List of constructor arguments.
   *
   * @var array
   */
  protected $_arguments = array();

  /**
   * Indicator if the object was used as array.
   *
   * @var bool
   */
  protected $_is_array = FALSE;

  /**
   * Collection of object properties or array values.
   *
   * @var array
   */
  protected $_properties = array();

  /**
   * Stores method calls as objects.
   *
   * @var array
   */
  protected $_method_calls = array();

  /**
   * Constructor
   *
   * @param string $class_name
   *   Name of the class this option implements.
   */
  function __construct($class_name = NULL) {
    // Store the class name of the constructor.
    if (isset($class_name)) {
      $this->_name = $class_name;
    }

    // Collect the arguments the constructor was called with.
    $args = func_get_args();
    if (count($args) > 1) {
      array_shift($args);
      $this->_arguments = $args;
    }
  }

  /**
   * Implements __get().
   *
   * @param $name
   *
   * @return \Drupal\features_yaml\ExportObject
   */
  function __get($name) {
    if (!isset($this->_properties[$name])) {
      // As long we get a element, that is not defined yet, we can be sure that
      // this should be a container.
      $this->_properties[$name] = new ExportObject();
    }

    return $this->_properties[$name];
  }

  /**
   * Implements __set().
   *
   * @param $name
   * @param $value
   */
  function __set($name, $value) {
    $this->_properties[$name] = $value;
  }

  /**
   * Implements __call().
   *
   * We store each method call in a
   *
   * @param string $name
   * @param array $arguments
   *
   * @return ExportMethod
   */
  function __call($name, $arguments) {
    $method = new ExportMethod($name, $arguments);
    $this->_method_calls[] = $method;
    return $method;
  }

  /**
   * Static call override to use as constructor.
   *
   * @param $method_name
   *   The method name will equal the original class name.
   * @param $arguments
   *   The arguments are the original constructor arguments.
   *
   * @return ExObject
   *   We return an ExObject so
   */
  public static function __callStatic($method_name, $arguments) {
    $class = get_called_class();
    $reflect  = new \ReflectionClass($class);
    // Pass name as first argument.
    array_unshift($arguments, $method_name);
    $instance = $reflect->newInstanceArgs($arguments);
    return $instance;
  }

  /**
   * Implements ArrayAccess::offsetSet().
   */
  public function offsetSet($offset, $value) {
    $this->_is_array = TRUE;

    if (is_null($offset)) {
      $this->_properties[] = $value;
    }
    else {
      $this->_properties[$offset] = $value;
    }
  }

  /**
   * Implements ArrayAccess::offsetExists().
   */
  public function offsetExists($offset) {
    return isset($this->_properties[$offset]);
  }

  /**
   * Implements ArrayAccess::offsetUnset().
   */
  public function offsetUnset($offset) {
    $this->_is_array = TRUE;

    unset($this->_properties[$offset]);
  }

  /**
   * Implements ArrayAccess::offsetGet().
   */
  public function offsetGet($offset) {
    $this->_is_array = TRUE;

    // If a offset is requested that is not yet a part of the array, we expect
    // it to be an ExportObject() so we can handle any call to that object,
    // regardless if it shall implement a function, object or array.
    if (!isset($this->_properties[$offset])) {
      $this->_properties[$offset] = new ExportObject();
    }
    return $this->_properties[$offset];
  }

  /**
   * Pseudo magic function to convert this object to a better array.
   *
   * @return array
   *
   * @see _features_yaml_recursive_object_to_array()
   */
  public function __toArray() {

    // If the object was treated as array, we simply return the properties.
    // In that case there also should not have been any calls to any method.
    if ($this->_is_array) {
      return $this->_properties;
    }

    // We build a special key for the constructor call.
    $constructor_call = $this->getConstructorCall();

    // We collect properties and methods as return values.
    $return = $this->_properties;
    if (count($this->_method_calls)) {
      $return['method calls'] = array();

      foreach ($this->_method_calls as $method) {

        // If the constructor can implement our getConstructorCall() we might
        // deal with the ExportMethod object and therefore replace our method
        // call array ke with the constructor call, to make it more readible.
        if (is_object($method)
          && method_exists($method, 'getConstructorCall')
          && method_exists($method, '__toArray')) {

          // Our constructor call as string - we use that as key.
          $method_constructor = $method->getConstructorCall();

          // If we got only one item, the key is the constructor call key.
          $arr = $method->__toArray();
          if (count($arr) == 1) {
            $arr = reset($arr);
          }

          $return['method calls'][$method_constructor] = $arr;
        }
        // Else we simply append the item.
        else {
          $return['method calls'][] = $method;
        }
      }
    }

    return ($constructor_call) ? array($constructor_call => $return) : $return;
  }

  /**
   * Helps to get the constructor command that might have built that object.
   *
   * @return string
   *   A string like "new ClassName(...)" to show how the inital call looked
   *   like.
   */
  public function getConstructorCall() {
    $call = 'new ' . $this->_name . '(';
    if (count($this->_arguments)) {
      $call .= json_encode($this->_arguments);
    }
    return $call . ')';
  }

}
