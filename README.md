This module provides a way to export feature's exports to multiple yml files
located in the feature's config folder. It is only exported for reference.

### Usage

The module can be used with drush in two ways:

* Using `drush features-update-yaml modulename` or `drush features-update-all-yaml`
  to directly invoke the config creation for a single module or all active
  feature modules.
* Use feature's `drush features-update modulename` or `drush features-update-all.
  So the config folder will automaticall be created after that features update
  command was executed.

### Background

It therefore uses the symfony [Yaml][1] component (currently 2.2.0) which is
located in `lib/symfony/yaml`. For each single component (e.g. a single view)
a single yml file is created. The content of that is only a representation of
the original features export, as that is parsed from the php-strings created by
the features_export_render_hooks. For fetching nested values and objects the
ExportObject and ExportMethod classes are used to create the export structure.

The exported yml files could be used to better track changes of features, as
less merge conflicts will occur, because each single component has its own file
and the exported yaml format is better to read.

With some more effort it maybe would be possible to reimport from those config
files, but that will not be part of this project/sandbox.

### Dependencies

*   [Features][2]
*   [Classloader][3]
*   [drush][4]

 [1]: https://github.com/symfony/Yaml
 [2]: http://drupal.org/project/features
 [3]: http://drupal.org/project/classloader
 [4]: http://drupal.org/project/drush

