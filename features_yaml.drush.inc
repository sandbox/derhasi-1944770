<?php

/**
 * @file
 * Drush implementations for features_yml.
 */

/**
 * Implements hook_drush_command().
 */
function features_yaml_drush_command() {
  return array(
    'features-update-yaml' => array(
      'description' => "Create or update the config folder with yaml files for the given feature module.",
      'arguments' => array(
        'feature' => 'A space delimited list of features.',
      ),
      'required-arguments' => TRUE,
      'drupal dependencies' => array('features'),
      'aliases' => array('fu-yml'),
    ),
    'features-update-all-yaml' => array(
      'description' => "Create or update the config folder with yaml files for all active feature modules.",
      'arguments' => array(
        'exclude' => 'A space delimited list of features to exclude from update.',
      ),
      'drupal dependencies' => array('features'),
      'aliases' => array('fua-yml'),
    ),
  );
}


/**
 * Implements drush_hook_post_features_update().
 */
function drush_features_yaml_post_features_update() {

  $modules = func_get_args();

  // If the features update passed one or more module names, we can start over
  // to create the config folders for those modules.
  if (!empty($modules)) {
    foreach ($modules as $module) {
      _drush_features_yaml_write_export_to_module($module);
    }
  }
}

/**
 * Drush callback for building yaml config folders for several features.
 */
function drush_features_yaml_features_update_yaml() {
  // Quit immediately if we got no root.
  $root = drush_get_option(array('r', 'root'), drush_locate_root());
  if (!$root) {
    drush_die(dt('Couldn\'t locate site root'));
  }

  // Get all modules from the arguments.
  $modules = func_get_args();
  // ... and process the export for that modules.
  if (!empty($modules)) {
    foreach ($modules as $module) {
      _drush_features_yaml_write_export_to_module($module);
    }
  }
}

/**
 * Drush callback for building yaml export files for all active features.
 *
 */
function drush_features_yaml_features_update_all_yaml() {

  $features_to_update = array();
  $features_to_exclude = func_get_args();
  foreach (features_get_features() as $module) {
    if ($module->status && !in_array($module->name, $features_to_exclude)) {
      $features_to_update[] = $module->name;
    }
  }
  drush_print(dt('YAML exports will be created for the following modules: @modules', array('@modules' => implode(', ', $features_to_update))));
  if (drush_confirm(dt('Do you really want to continue?'))) {
    foreach ($features_to_update as $module_name) {
      drush_invoke_process(drush_sitealias_get_record('@self'), 'features-update-yaml', array($module_name));
    }
  }
  else {
    drush_die('Aborting.');
  }


}


/**
 * Helps to write the features export to config for a single features module.
 *
 * @param string $module_name
 *   Machine name of the feature module.
 */
function _drush_features_yaml_write_export_to_module($module_name) {

  $export = features_yaml_feature_export_as_yml($module_name);
  if (empty($export)) {
    drush_log(dt('Features YAML: No export available for @module.', array('@module' => $module_name)));
    return;
  }

  // We place the files within the module's config folder.
  $directory = drupal_get_path('module', $module_name);
  $directory .= '/config';

  // If the folder exists we remove the old yml files (after prompt).
  if (is_dir($directory)) {
    drush_print(dt('Config folder appears to already exist in @dir', array('@dir' => $directory)));
    if (!drush_confirm(dt('Do you really want to continue and overwrite all existing yml files in that folder?'))) {
      drush_die('Aborting.');
    }

    $existing_files = drush_scan_directory($directory, '/\.yml$/is');
    foreach ($existing_files as $filename => $file) {
      drush_op('unlink', $filename);
    }
  }
  // Else we try to create the folder.
  else {
    drush_op('mkdir', $directory);
  }

  // If the directory is present, we can write the yml files to that.
  if (is_dir($directory)) {
    foreach ($export as $filename => $content) {
      drush_op('file_put_contents', "{$directory}/{$filename}.yml", $content);
    }
    drush_log(dt("Features YAML: Created config for @module in @directory", array('@module' => $module_name, '@directory' => $directory)), 'ok');
  }
  else {
    drush_die(dt('Couldn\'t create directory @directory for creating the configuration.', array('@directory' => $directory)));
  }
}

